'Variables Declaration
Const StrProjNam = "EasyNet"'Name of the  project that is related to the Data file, Object Repository file and so on
Environment.Value("RunID") = "Run-" & Environment.Value("LocalHostName") &"-1"


'#################################### UploadFilesToQC #####################################
'-----------------------------------------------------------------------------------------------------------------------------
'Function Name     :   UploadFilesToQC
'Parameter Input   :   strQCPath, strFilesystemPath
'Description           :   Upload Files To QC
'Calls                       :   None
'Return Value       :   None
'Author					   :   Hiranmayee Kesari
'-----------------------------------------------------------------------------------------------------------------------------

Public Function UploadFilesToQC(strQCPath, strFilesystemPath, strQCFolder)  

   If Environment.Value("ExecutionType")<>"QC" Then
		Exit Function
	End If

	Dim fileCount
	fileCount = 0

	'Get QC connection object
	Set QCConnection = QCutil.TDConnection

	'Get Test Plan tree structure
	Set treeManager = QCConnection.TestSetTreeManager
	Set node = treeManager.NodeByPath(strQCPath)
    Set AFolder = node.FindChildNode(strQCFolder) 
	Set oAttachment = AFolder.Attachments
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set oFolder = fso.GetFolder(strFilesystemPath)
	Set oFiles = oFolder.Files

	'Iterate through each file present in File System path
	If oFiles.count >0 Then
		For Each oFile in oFiles
			Set attach = oAttachment.AddItem(Null)			
			attach.FileName = oFile.Path
			attach.Type = 1
			attach.Post()
			fileCount = fileCount + 1

			Set attach = Nothing
		Next        	

		Reporter.ReportEvent micPass, "In ALM ", "'" & fileCount & "' files are uploaded."
		Call ReportHTMLResult  ("Function : UploadFilesToQC","", "Result files should be uploaded in ALM.", "" ,"Total files uploaded in ALM : " & fileCount, "Passed")
	Else
		Reporter.ReportEvent micFail,"File NOT found.", "No file found at path : " & strFilesystemPath
		Call ReportHTMLResult  ("Function : UploadFilesToQC","", "Result files should be uploaded in ALM.", "" ,"No file found at path : " & strFilesystemPath, "Failed")
	End If

	Set QCConnection = Nothing
	Set treeManager = Nothing
	Set node = Nothing
	Set AFolder = Nothing
	Set oAttachment = Nothing		
	Set fso = Nothing
	Set oFolder = Nothing
	Set oFiles = Nothing

End Function


'########################### QCCreateExecutionFolder #######################################
'-----------------------------------------------------------------------------------------------------------------------------
'Function Name      :   QCCreateExecutionFolder
'Parameter Input    :   None
'Description            :   Creates Execution Folder in Test Lab
'Calls                        :   None
'Return Value        :   None
'Author					    :   Hiranmayee Kesari
'-----------------------------------------------------------------------------------------------------------------------------

Function QCCreateExecutionFolder()
	
	If Environment.Value("ExecutionType")<>"QC" Then
		Exit Function
	End If
	
	Dim  Otdc, OTSTreeMang, ORoot, OExecFld, OHTMLFld, OScrnShtFld, OSubRootFld, OSubExecFld, ODateFld, OSubDateFld, ORunFld
	Const StrProjNam = "EasyNet"

	BoolFldPresent = False
	BoolExecFldPresent = False
	BoolRunFldPresent = False

	strDateFoldername = Year(Date) & "-" & Right("00" & Month(Date),2) & "-" & Right("00" & Day(Date),2) 'Changed by K
	'strDateFoldername = Day(Date) & "-" & MonthName(Month(Date)) & "-" & Year(Date)
	
	Set Otdc = QCutil.TDConnection
	Set OTSTreeMang = Otdc.TestSetTreeManager
	Set ORoot = oTSTreeMang.root
	
	ORoot.Refresh
	Set OSubRootFld = ORoot.SubNodes
	
	For Each OExecFld In OSubRootFld
		If OExecFld.Name = StrProjNam&" Automation Scripts-Execution Logs" Then
			Set OExecFld = ORoot.FindChildNode(StrProjNam&" Automation Scripts-Execution Logs")
			BoolExecFldPresent = True
			Exit For
		End If
	Next
	
	
	If Not BoolExecFldPresent Then
		Set OExecFld = ORoot.AddNode(StrProjNam&" Automation Scripts-Execution Logs")
	End If

	Set OSubExecFld = OExecFld.SubNodes
	
	For Each ODateFld In OSubExecFld
		If ODateFld.Name = StrDateFoldername Then
			Set ODateFld = OExecFld.FindChildNode(strDateFoldername)
			BoolDateFldPresent = True
			Exit For
		End If
	Next
	
	If Not BoolDateFldPresent Then
		Set ODateFld = OExecFld.AddNode(strDateFoldername)
	End If
	
	RunID = Environment.Value("RunID")
	Set OSubDateFld = ODateFld.SubNodes

	For Each ONode In OSubDateFld
		If ONode.Name = RunID Then			
			ONode.Name = "Backup_" & oNode.Name & "(" & Hour(Time) & "-" & Minute(Time) & ")"
			ONode.Post
			ONode.Refresh
			Exit For
		End If
	Next

	For Each ONode In OSubDateFld
		If ONode.Name = RunID Then			
			BoolRunFldPresent = True
			Exit For
		End If
	Next

	If OSubDateFld.Count <> 0 And BoolRunFldPresent Then
		Set ORunFld = ODateFld.FindChildNode(RunID)
	Else
		Set ORunFld =ODateFld.AddNode(RunID)
	End If

	Environment.Value("strRunFolderPath") = "Root\" & StrProjNam & " Automation Scripts-Execution Logs\" & strDateFoldername
	Environment.Value("strRunFolderName") = ORunFld.Name

	Set Otdc = Nothing
	Set OTSTreeMang = Nothing
	Set ORoot = Nothing
	Set OSubRootFld = Nothing
	Set OExecFld = Nothing
	Set OSubExecFld = Nothing
	Set ODateFld = Nothing
	Set OSubDateFld = Nothing
	Set ORunFld = Nothing
	Set OHTMLFld = Nothing
	Set OScrnShtFld = Nothing
	Set ONode = Nothing
	
End Function

'########################### CreateZipFolder #######################################
'-----------------------------------------------------------------------------------------------------------------------------
'Function Name      :   CreateZipFolder
'Parameter Input    :   None
'Description            :   Creates Zip Folder of Execution Results in Test Lab
'Calls                        :   None
'Return Value        :   None
'Author					    :   Hiranmayee Kesari
'----------------------------------------------------------------------------------------------------------------------------

Function CreateZipFolder(strRequiredFolder, strZipFolder)

	On Error Resume Next
   
'	Dim objApp, objFolder, objFSO, objItem, objTxt
'	Dim strSkipped
'	
'	Const ForWriting = 2
'	intSkipped = 0
'	
'	' Create an empty ZIP file
'	Set objFSO = CreateObject( "Scripting.FileSystemObject" )
'	
'	If  objFSO.FileExists(strZipFolder)Then
'		objFSO.DeleteFile strZipFolder, True
'	End If
'	
'	Set objTxt = objFSO.OpenTextFile( strZipFolder, ForWriting, True )
'
'	objTxt.Write Chr(80) & Chr(75) & Chr(5) & Chr(6) & String( 18, Chr(0) )
'	objTxt.Close
'
'	Set objTxt = Nothing
'	
'	' Create a Shell object
'	Set objApp = CreateObject( "Shell.Application" )
'
'	' Copy the files to the compressed folder
'	For Each objItem in objApp.NameSpace( strRequiredFolder ).Items
'		If objItem.IsFolder Then
'		' Check if the subfolder is empty, and if so, skip it to prevent an error message
'			Set objFolder = objFSO.GetFolder( objItem.Path )
'			If objFolder.Files.Count + objFolder.SubFolders.Count = 0 Then
'				intSkipped = intSkipped + 1
'			Else
'				objApp.NameSpace( strZipFolder ).CopyHere objItem
'			End If
'		Else
'			objApp.NameSpace( strZipFolder ).CopyHere objItem
'		End If
'	Next
'
'	Set objFolder = Nothing
'	Set objFSO    = Nothing
'
'	' Keep script waiting until compression is done
'	intSrcItems = objApp.NameSpace( strRequiredFolder  ).Items.Count
'
'	Do Until objApp.NameSpace( strZipFolder ).Items.Count + intSkipped = intSrcItems
'		WScript.Sleep 200
'	Loop
'
'	Set objApp = Nothing


     ' Declare the required variables
    Dim objZip, objSA, objFolder, zipFile, FolderToZip, Counter
    
	If  CreateObject("Scripting.FileSystemObject").FileExists(strZipFolder)Then
		CreateObject("Scripting.FileSystemObject").DeleteFile strZipFolder, True
	End If
 
    'Create the basis of a zip file.
    CreateObject("Scripting.FileSystemObject").CreateTextFile(strZipFolder, True).Write "PK" & Chr(5) & Chr(6) & String(18, vbNullChar)
 
    ' Create the object of the Shell
    Set objSA = CreateObject("Shell.Application") 
    
    ' Add the folder to the Zip
    Set objZip = objSA.NameSpace(strZipFolder) 
    Set objFolder = objSA.NameSpace(strRequiredFolder) 
    objZip.CopyHere(objFolder.Items) 
    
	'Wait till compressing
	Wait(15)

	Counter = 1

	Do Until objSA.NameSpace( strZipFolder ).Items.Count = objSA.NameSpace( strRequiredFolder  ).Items.Count
		Wait(20)
		Counter = Counter + 1
		If Counter > 4 Then
			Exit Do
		End If
	Loop


	Set objSA = Nothing
	Set objZip = Nothing
	Set objFolder = Nothing
	
End Function

Function QCGetFile(ByVal strFileName, ByVal strQCPath)
	Set QcConn= QCutil.QCConnection 
	Set QcTreeMagr = QcConn.treemanager
			StrQCRootStructure = strQCPath
	Set QcFldr = QcTreeMagr.NodebyPath(StrQCRootStructure)
	Set QcAttach = QcFldr.attachments
	Set QcAttachList = QcAttach.NewList("")
	Set AttRef1 = GetAttachRef(StrFileName, QcAttachList)
			QCGetFile =  AttRef1.Filename
	Set QcConn = Nothing
	Set QcTreeMagr = Nothing
	Set QcFldr = Nothing
	Set QcAttach = Nothing
	Set QcAttachList = Nothing
	Set AttRef1 = Nothing
End Function

Function GetTextFileFromQC(strFileName,strQCPath)
	strTextFile = QCGetFile(strFileName,strQCPath)
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objTxtFile = objFSO.OpenTextFile(strTextFile,1,true,-2)
	GetTextFileFromQC = objTxtFile.ReadAll()
End Function

Function GetFileFromQC(strQCPath, strFileName, intOverwrite)
	Set FSO = CreateObject("Scripting.FileSystemObject")
	Set QcConn= QCutil.QCConnection 
	Set QcTreeMagr = QcConn.treemanager
	Set QcFldr = QcTreeMagr.NodebyPath(strQCPath)
	Set QcAttach = QcFldr.attachments
	Set QcAttachList = QcAttach.NewList("")
		strPath = FSO.GetParentFolderName(QcAttachList(1).FileName)
		For each RefAtt in QcAttachList
			strName = strPath & "\" & RefAtt.name(1)
			If Not FSO.FileExists(strName) Then
				RefAtt.Load true,""
				RefAtt.Refresh
				FSO.MoveFile RefAtt.Filename, strName
			Else
				If intOverwrite = 1 Then
					FSO.DeleteFile strName,true
					RefAtt.Load true,""
					RefAtt.Refresh
					FSO.MoveFile RefAtt.Filename, strName
				End If
			End If
			If strFileName = RefAtt.name(1) Then
				GetFileFromQC = strName
			End If
		Next
	Set QcConn = Nothing
	Set QcTreeMagr = Nothing
	Set QcFldr = Nothing
	Set QcAttach = Nothing
	Set QcAttachList = Nothing
	Set FSO = Nothing
End Function
